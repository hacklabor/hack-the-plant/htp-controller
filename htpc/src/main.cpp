#include <Arduino.h>
#include <main.h>

unsigned long oldMillis;
int statusA,statusB;


void Wifi_ElegantOTA()
{
  WiFi.mode(WIFI_STA);
  WiFi.setHostname(STAHOST);
  WiFi.begin(STASSID, STAPSK);
  WiFi.setHostname(STAHOST);

  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    // Serial.println("Connection Failed! Rebooting...");
    delay(5000);
  }

    server.on("/", []() {
    String text="Hi! I am esp-HackThePlant \n OTA Update unter http://[hostname]/update A :"+String(statusA)+" B:"+String(statusB);
    server.send(200, "text/plain",text);
  });


  ElegantOTA.begin(&server); // Start ElegantOTA
  server.begin();
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
  LED_G.setDutyCycle(10 );
  LED_RB.setDutyCycle(10);
  
  
  Serial.begin(115200);
  pinMode(D2, INPUT);
  Wifi_ElegantOTA();
  timeClient.begin();
  timeClient.setTimeOffset(3600); // Set time offset to GMT+1
  timeClient.setUpdateInterval(60000);
  timeClient.update();

  LED_G.setDutyCycle(100);
  LED_RB.setDutyCycle(100); 
  delay(2000);
 LED_G.setDutyCycle(0);
  LED_RB.setDutyCycle(0);
}

void loop() {
  
  if (digitalRead(D2)){
    LED_G.setDutyCycle(0);
    LED_RB.setDutyCycle(0); 
    oldMillis=millis();
    statusA=1;
  }
  else{
        // LED_G.setDutyCycle(power_Licht_G);
  LED_RB.setDutyCycle(power_Licht_RB); 
  statusA=2;
  }
  timeClient.update();
  server.handleClient();
  
  int h = timeClient.getHours();
  int m = timeClient.getMinutes();
  int s = timeClient.getSeconds();
  Serial.print(h);
  Serial.print(':');
  Serial.print(m);
  Serial.print(':');
  Serial.println(s);
  if (h>=6 && h<22){
     power_Licht_RB = 100;
      power_Licht_G = 100; 
      statusB=1; 
  }
   else{
    if (millis()-oldMillis>1000){
      power_Licht_RB = 00;
      power_Licht_G = 00;
       statusB=2;   }
      else{
      power_Licht_RB = 20;
      power_Licht_G = 20; 
       statusB=3; 
      }
  }
  delay(10);
}