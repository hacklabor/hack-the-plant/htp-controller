#include "Pwm.h"
#include <Arduino.h>

Pwm::Pwm(int pin,int frequenz) {
  this->pin = pin;
  pinMode(pin, OUTPUT);
  analogWriteFreq (frequenz); // Set PWM frequency to 1kHz
}

void Pwm::setDutyCycle(int dutyCycle) {
  this->dutyCycle = dutyCycle;
  int dc = dutyCycle*2.55;
  if (dutyCycle==100){
    dc=256;
  }
  analogWrite(pin, dc);
}

int Pwm::getDutyCycle() {
  return dutyCycle;
}
