#pragma once


#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <TimeTrigger.h>
#include <ElegantOTA.h>
#include <secrets.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Pwm.h>



//OTA

ESP8266WebServer server(80);

//Licht LED
Pwm LED_RB(D5,5000);
Pwm LED_G(D6,5000);

//NTP
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

int power_Licht_RB = 0;
int power_Licht_G = 0;