#ifndef PWM_H
#define PWM_H
#include <Arduino.h>
class Pwm {
  public:
    Pwm(int pin,int frequenz);
    void setDutyCycle(int dutyCycle);
    int getDutyCycle();
  private:
    int pin;
    int dutyCycle;
};

#endif
