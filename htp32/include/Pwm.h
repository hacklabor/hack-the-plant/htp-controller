#ifndef PWM_H
#define PWM_H
#include <Arduino.h>
class Pwm {
  public:
    Pwm(int chanel, int pin,int frequenz,int resulution);
    void setDutyCycle(int dutyCycle);
    int getDutyCycle();
  private:
    int pin;
    int chanel;
    int resolution;
    int dutyCycle;
};

#endif
