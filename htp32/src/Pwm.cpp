#include "Pwm.h"
#include <Arduino.h>

Pwm::Pwm(int chanel, int pin,int frequenz,int resulution) {
  this->pin = pin;
  this->chanel = chanel;
  this->resolution = resolution;
  ledcSetup(chanel, frequenz, resolution);
  ledcAttachPin(pin,chanel);
 
}

void Pwm::setDutyCycle(int dutyCycle) {
  this->dutyCycle = dutyCycle;
  if (dutyCycle>0){
  ledcAttachPin(pin,chanel);
  ledcWrite(pin, dutyCycle*(2^resolution/100));
  }
  else{
    ledcDetachPin(pin);
  }
}

int Pwm::getDutyCycle() {
  return dutyCycle;
}
